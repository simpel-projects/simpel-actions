from django.contrib import admin
from django.contrib.admin import action

from example.app.models import SalesOrder
from simpel_actions.admin import ActionableAdminMixin
from simpel_actions.handlers import ActionHandler


class SalesOrderActionHandler(ActionHandler):

    @action(permissions=["validate", "makan"], description="Validate ")
    def do_validate(self, modeladmin, request, queryset):
        print(modeladmin)
        pass

    def can_validate(self, request, obj=None):
        return True


class SalesOrderAdmin(ActionableAdminMixin):
    action_handler_class = SalesOrderActionHandler
    actions = ["validate_action"]


admin.site.register(SalesOrder, SalesOrderAdmin)
