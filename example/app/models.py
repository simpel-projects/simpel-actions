from django.db import models
from django.utils.translation import gettext_lazy as _

from simpel_actions.models import PendingAction, ValidateAction


class SalesOrderActionMixin(PendingAction, ValidateAction):
    PENDING = 1
    VALID = 2
    STATUS_CHOICES = (
        (PENDING, _("Pending")),
        (VALID, _("Valid")),
    )

    status = models.IntegerField(_("status"), choices=STATUS_CHOICES, default=PENDING)

    class Meta:
        abstract = True


class SalesOrder(SalesOrderActionMixin):

    uid = models.IntegerField()
